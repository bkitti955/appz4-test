import { Icon } from '../interfaces/icon';

export const ICONS: Icon[] = [
  {
    src: './assets/google.png',
    alt: 'Google+'
  },
  {
    src: './assets/summer-school.png',
    alt: 'Summer school'
  },
  {
    src: './assets/skype.png',
    alt: 'Skype'
  },
  {
    src: './assets/twitter.png',
    alt: 'Twitter'
  },
  {
    src: './assets/fire.png',
    alt: 'Fire'
  }
]
