import { Card } from '../interfaces/card';

export const CARDS: Card[] = [
  {
    id: 'A',
    name: 'solutions',
    description: 'Lalesua kertase meyuade Odioner grda aselerade magnrem bus jertyadesede saleget kelems.',
    buttonTitle: 'more'
  },
  {
    id: 'B',
    name: 'analysis',
    description: 'Kerterasde nsera fertades Odioner gaskase lertade magnrem leotbus jertyadese eget kelemenr sus.',
    buttonTitle: 'more'
  },
  {
    id: 'C',
    name: 'strategies',
    description: 'Lalesua kertase meyuade Odioner grda aselerade magnrem bus jertyadesede saleget kelems.',
    buttonTitle: 'more'
  },
  {
    id: 'D',
    name: 'development',
    description: 'Kerterasde nsera fertades Odioner gaskase lertade magnrem leotbus jertyadese eget kelemenr sus.',
    buttonTitle: 'more'
  }
]
