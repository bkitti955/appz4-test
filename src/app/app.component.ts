import { Component } from '@angular/core';
import { CARDS } from "./mocks/mock-cards";
import { ICONS } from "./mocks/mock-icons";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  cards = CARDS;
  icons = ICONS;
  title = 'sitebuild';
}
